﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Session_06
{
    class Program
    {
        static void Main(string[] args)
        {
            //01.Create a New Folder 
            //02.Naming a Folder
            //string path = @"C:\Users\mohamed.arshad\Desktop\Session_06";
            //string folderPath = WinOperations.CreateFolder(path, "Automated Folder");


            ///03. Create a File (.txt)
            ///04. Save that file in newly created Folder
            //string name = "Automated File";
            //string content = "Hii Guys!!! All Fine";

            //WinOperations.CreateTextFile(folderPath, name, content);

            ///05. How to copy from one location to another
            //string oldLocation = @"C:\Users\mohamed.arshad\Desktop\Session_06\Automated Folder\Automated File.txt";

            //string newLocation = @"C:\Users\mohamed.arshad\Desktop\Session_06";

            //WinOperations.FileCopy(oldLocation, newLocation);


            //06. Delete Folder
            string folderFullName = @"C:\Users\mohamed.arshad\Desktop\Session_06\Automated Folder";
            WinOperations.DeleteFolder(folderFullName);

        }
    }
}
