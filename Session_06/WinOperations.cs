﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Session_06
{
    class WinOperations
    {
        public static string CreateFolder(string path, string folderName)
        {
            //Directory
            string fullName = path + "\\" + folderName;
            DirectoryInfo info = Directory.CreateDirectory(fullName);
            return info.FullName;
        }

        public static void CreateTextFile(string folderPath, string name, string content)
        {
            //StreamWriter
            string fullName = folderPath + "\\" + name + ".txt";

            StreamWriter writer = new StreamWriter(fullName);
            writer.WriteLine(content);
            writer.Dispose();

        }

        public static void FileCopy(string oldLocation, string newLocation)
        {
            //File Name
            string fileName = Path.GetFileName(oldLocation);

            //New Location
            string newLoc = newLocation + "\\" + fileName;

            File.Copy(oldLocation, newLoc);
        }

      
    }
}
