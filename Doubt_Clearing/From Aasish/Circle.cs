﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPS
{ 
    class Circle
    {
        public double Radius = 0;
        
        public Point CenterPoint;

        public double Perimeter()
        {
            return Math.PI * 2 * Radius; 
        }
        
        public double Area()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }


        //public void Display (double area)
        //{
        //    Console.WriteLine("Area of Circel: " + area);
        //}

        //public void Display(double perimeter)
        //{
        //    Console.WriteLine( "Perimeter of Circel: " + perimeter);
        //}

        public void Display(double value, string areaOrPerimeter)
        {
            Console.WriteLine(areaOrPerimeter+" of Circel: " + value);
        }
    }
}
