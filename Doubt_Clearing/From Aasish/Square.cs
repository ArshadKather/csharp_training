﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPS
{
    internal class Square
    {
        public double Side = 0;

        public Point CenterPoint;

        public double Perimeter()
        {
            return Side*4;
        }

        public double Area()
        {
            return  Math.Pow(Side, 2);
        }


        public void Display(double Area)
        {
            Console.WriteLine("Area of Square: " + Area);
        }

        //public void Display (double Perimeter)
        //{
        //    Console.WriteLine("Perimeter of Area: " + Perimeter);
        //}

    }
}






