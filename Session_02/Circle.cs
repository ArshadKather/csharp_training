﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_02
{
    class Circle
    {
        //Properties of Circle
        public double radius = 0;
        public double x=0;
        public double y=0;

        //Location
        //public Point centerpoint;

        //Method
        public double Area()
        {
            return Math.PI * Math.Pow(radius, 2);
        }

        public double Perimeter()
        {
            return 2 * Math.PI * radius;
        }

        public void DisplayArea(double area)
        {
            Console.WriteLine("Area of Circle : " + area);
        }

        public void DisplayPerimeter(double perimeter)
        {
            Console.WriteLine("Perimeter of Circle : " + perimeter);
        }
    }
}
