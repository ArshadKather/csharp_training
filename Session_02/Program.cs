﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_02
{
    class Program
    {
        static void Main(string[] args)//Method Start point for Console App
        {

            //School Bag
            //Data Members
            //Methods
            //Books
            //Pen
            //Lunch Box

            //Area of the Circle

            #region Normal procedure

            //double radius = 10;

            //double area = Math.PI * Math.Pow(radius, 2);

            //Console.WriteLine("Area : " + area);


            #endregion

            #region OOP

            //Column - Circle

            Circle column = new Circle();
            column.radius = 100;
            column.x = 0;
            column.y = 0;

            double area = column.Area();
            double perimeter = column.Perimeter();

            column.DisplayArea(area);
            column.DisplayPerimeter(perimeter);

            //Circle column02 = new Circle();



            #endregion


        }
    }



}
