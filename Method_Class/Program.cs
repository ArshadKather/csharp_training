﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Method_Class
{
    class Program
    {
        static void Main()
        {
            //Which Add Two Number (Input 2 number out --> addition value)

            //What is Method or Function ?
            //To reduce of Lines of Code

            //First Name
            //Second Name
            //Full==> FN+SN

            Program p = new Program();

            //Get User Input
            string[] userInputs = p.GetUserInput();

            //Name Join
            string fullName = p.NameJoin(userInputs[0], userInputs[1]);

            //Display
            p.Display(fullName);

            Console.ReadKey();

        }

        public void JoinName()
        {
            Console.Write("Enter First Name : ");
            string fName = Console.ReadLine();

            Console.Write("Enter Second Name : ");
            string sName = Console.ReadLine();

            Console.WriteLine("Full Name: " + fName + " " + sName);
        }

        public string NameJoin(string fName, string sName)
        {
            return fName + " " + sName;
        }

        public string[] GetUserInput()
        {
            string[] inputs = new string[2];

            Console.Write("Enter First Name : ");
            inputs[0] = Console.ReadLine();

            Console.Write("Enter Second Name : ");
            inputs[1] = Console.ReadLine();

            return inputs;
        }

        public void Display(string fullName)
        {
            Console.WriteLine("Full Name: " + fullName);
        }

    }





}
