﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_04
{
    class Health
    {

        int healthPercentge = 0;

        public string demo { get; set; }

        public int Healthy
        { 

            get
            {
                return healthPercentge;
            }

            set
            {
                if (value > 0 && value < 10)
                {
                    healthPercentge = 0;
                }
                else if (value > 10 && value < 50)
                {
                    healthPercentge = 50;
                }
                else if (value > 50)
                {
                    healthPercentge = 100;
                }
            }
        }

        public void DisplayHealthPercentage()
        {
            Console.WriteLine("your health percentage : " + healthPercentge);
        }
    }
}
