﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_03
{
    class Circle
    {
        double radius = 0;

        //Constructor
        public Circle(double radius)
        {
            this.radius = radius;
        }

        //Method/Function ---> Need to Give Input and it will gives you a output
        public double Area()
        {
            return  Math.PI * Math.Pow(radius, 2);
        }

        public double Perimeter()
        {
            return Math.PI * 2* radius;
        }

        public void SetRadius(double radius)
        {
            this.radius = radius;
        }
    }
}
