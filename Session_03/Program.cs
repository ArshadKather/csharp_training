﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_03
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Without Constructor

            //Circle obj = new Circle();
            ////obj.radius = 10;
            ////obj.SetRadius(10);
            //double area = obj.Area();

            //Console.WriteLine(area);

            #endregion

            #region With Constructor

            Circle obj = new Circle(10);
            double area= obj.Area();

            Console.WriteLine(area);


            #endregion


            Console.ReadKey();
        }
    }
}
